class Rangos:

    def getTotalElements(cadena):
        tamano = len(cadena)
        total = tamano;
        if  tamano > 1:
            if "," in cadena:
                numeros = cadena.split(",")
                total = len(numeros);
        return total;


    def getMax(numeros):
        return max(numeros);

    def getMin(numeros):
        return min(numeros);

    def getPromedio(numeros):
        total = 0;
        for ltr in numeros:
            total = total + int(ltr);
        return total / len(numeros);

    def getNumeroElementos(cadena):
        datos = [];
        if cadena is not None:
            valor = Rangos.getTotalElements(cadena);
            datos.append(valor);
            if "," in cadena:
                numeros = cadena.split(",")
                datos.append(Rangos.getMin(numeros));
                datos.append(Rangos.getMax(numeros));
                datos.append(Rangos.getPromedio(numeros));
            else:
                datos.append(len(cadena));
                datos.append(int(cadena));
            return datos;
        else:
            return "sin numeros";
